/* jshint esversion: 6 */
// On a ajouté la ligne ci-dessus pour éviter le incompatibilité avec jshint

const imagesMenu = document.querySelector("aside");
const canva = document.querySelector("#poster");

// Nombre d'images à récupérer de l'API
const IMAGENUM = 6;
// Clé d'accès à l'API
const CLIENT_ID = "f_exxCUSGPXBKxPzzsYVfSVpA_N6STcMje2TW0mwq7Y";

const url = `https://api.unsplash.com/photos/random/?client_id=${CLIENT_ID}&count=${IMAGENUM}`;

// Récupération des images de l'API
fetch(url)
  .then((res) => res.json())
  .then((data) => showData(data))
  .catch((e) => noData(e));

// Fonction pour afficher les images si l'API fonctionne
function showData(data) {
  data.forEach((item) => {
    let img = document.createElement("img");
    img.classList.add("importedImage");
    img.setAttribute("src", item.urls.full);
    imagesMenu.appendChild(img);
  });
}

// Fonction pour afficher des images par défaut si l'API ne fonctionne pas
function noData(e) {
  for (let i = 0; i < IMAGENUM; i++) {
    let img = document.createElement("img");
    img.classList.add("importedImage");
    img.setAttribute("src", `./img/img${i + 1}.jpg`);
    imagesMenu.appendChild(img);
  }
}

function createDropZones(number) {
  // Création des zones de dépôt
  for (let i = 0; i < number; i++) {
    // Création d'un div pour la zone de dépôt
    let dropZone = document.createElement("div");
    dropZone.classList.add("depot");
    dropZone.style.backgroundImage = "./img/img1.jpg"; // Chemin de l'image de fond

    // Ajout d'un titre à la zone de dépôt
    let zoneTitle = document.createElement("h2");
    zoneTitle.innerText = `Zone ${i + 1}`;
    dropZone.appendChild(zoneTitle);

    // Ajout de la zone de dépôt au canvas
    canva.appendChild(dropZone);
  }
}

// Fonction pour remplacer l'image dans une zone de dépôt
function dropImage() {
  // Écouteur d'événement pour sélectionner une zone de dépôt
  canva.addEventListener("click", (e) => {
    stopSelection();
    let target = e.target.closest(".depot"); // Utilisation de closest pour simplifier la sélection
    if (target) {
      target.classList.add("selected");
    }
  });

  // Ajout de l'événement pour détecter le dragover
  canva.addEventListener("dragover", (e) => {
    e.preventDefault(); 
    let target = e.target.closest(".depot");
    if (target) {
      target.classList.add("selected");
    }
  });

  // Ajout de l'événement pour détecter le drop
  canva.addEventListener("drop", (e) => {
    e.preventDefault();
    let selectedZone = document.querySelector(".selected");
    if (selectedZone) {
      let dataURL = e.dataTransfer.getData("text/plain"); // Récupérer les données "dragged" en tant que texte

      // Verification URl
      if (dataURL.includes("https")){
      // Création et ajout de la nouvelle image
      let img = new Image();
      img.classList.add("depotImage");
      img.src = dataURL;
      selectedZone.innerHTML = ""; // Supprimer le contenu de la zone de dépôt
      selectedZone.appendChild(img);
      selectedZone.classList.remove("selected");
      }
    }
  });

  // Écouteur d'événement pour changer l'image dans la zone sélectionnée
  imagesMenu.addEventListener("click", (e) => {
    if (e.target.matches(".importedImage")) {
      let selectedZone = document.querySelector(".selected");
      if (selectedZone) {
        // Création et ajout de la nouvelle image
        let img = document.createElement("img");
        img.classList.add("depotImage");
        img.src = e.target.src;
        selectedZone.innerHTML = "";
        selectedZone.appendChild(img);
      }
    }
  });
}

// Fonction pour désélectionner toutes les zones de dépôt
const stopSelection = () => {
  document.querySelectorAll(".depot").forEach((item) => {
    item.classList.remove("selected");
  });
};

createDropZones(4); // Appel de la fonction pour créer les zones de dépôt
dropImage(); // Appel de la fonction principale
