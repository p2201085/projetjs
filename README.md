# Photogrid
Ce projet Photogrid en JavaScript a été réalisé par Gatta BA et Abd-el-rahman GHERIBI à l'IUT Lyon 1. Il permet de récupérer des images à partir de l'API d'Unsplash et de les afficher dans des boîtes sur un canevas.

## Fonctionnalités
Les fonctionnalités bonus ajoutées sont les suivantes :

Rendre les zones redimensionnables par l'utilisateur.
Mettre en place un drag and drop.
Améliorer les parties HTML et CSS.

## Utilisation
Ouvrez le projet dans votre navigateur.
Cliquez sur le bouton "Récupérer des images" pour charger les images depuis l'API d'Unsplash.
Utilisez les fonctionnalités de redimensionnement et de glisser-déposer pour organiser les images sur le canevas.
Profitez de votre photogrid personnalisé !